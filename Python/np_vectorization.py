import numpy as np

def myfunc(a, b):
    "Return a - b if a>b, otherwise return a + b"
    if a > b:
        return a - b
    else:
        return a + b

vfunc = np.vectorize(myfunc)
vfunc([1,2,3,4], 2)

# vectorization은 python의 map function과 같은 역할을 한다.
# 성능을 위한게 아니라 편의를 위해 제공됨.
# 구현은 for문임.