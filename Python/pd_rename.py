import pandas as pd
df = pd.DataFrame({'oldName1': [2,3,4,5,8], 'oldName2': [9,33,45,52,84]})
df.rename(columns={'oldName1': 'newName1', 'oldName2': 'newName2'}, inplace=True)