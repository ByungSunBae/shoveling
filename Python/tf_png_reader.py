import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import os
import sys

labels = os.listdir("./phd08_png/")
phd08_png_list = ["./phd08_png/" + lab for lab in labels]

os.path.abspath("./phd08_png/")

def paste_abs_path(file, prev_dir):
    """Paste prev_dir to file.
    Args:
        files: string, file string in prev_dir.
        prev_dir: directory containing files.
    
    Returns:
        prev_dir + file ==> absolute path.
    """
    return str(prev_dir) + "/" + file

vpaste_abs_path = np.vectorize(paste_abs_path)
dirs_by_label = {labels[i] : vpaste_abs_path(os.listdir(phd08_png_list[i]), phd08_png_list[i]) for i in range(0, len(phd08_png_list))}

filename_queue = tf.train.string_input_producer(dirs_by_label[labels[0]].tolist())

reader = tf.WholeFileReader()
key, value = reader.read(filename_queue)

my_img = tf.image.decode_png(value) # use png or jpg decoder based on your files.

init_op = tf.global_variables_initializer()

image_list = []

with tf.Session() as sess:
    sess.run(init_op)
    # Start populating the filename queue.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    
    for i in range(4): #length of your filename list
        image_list.append(my_img.eval()) #here is your image Tensor :) 
    coord.request_stop()
    coord.join(threads)

plt.imshow(image_list[3])
plt.show()

# tensorflow의 reader를 이용하여 여러 png 파일을 불러와서 queue에 적재할 수 있음.