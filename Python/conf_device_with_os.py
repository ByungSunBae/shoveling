# tensorflow는 gpu가 1개든 여러개든 메모리를 전부 잡아놓는다.
# 본 파일에서는 os module을 통해 환경변수를 셋팅하여 
# 어떤 gpu를 사용할 것인지 설정하는 방법을 보여줄 것이다.
import os
import tensorflow as tf

try:
    print("Current number of CUDA devices ", os.environ['CUDA_VISIBLE_DEVICES'])
except KeyError as e:
    print("CUDA device is not visible")

os.environ['CUDA_VISIBLE_DEVICES'] = "0"
print("After change CUDA device ", os.environ['CUDA_VISIBLE_DEVICES'])